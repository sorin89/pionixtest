/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.bootstrap.html
 */

module.exports.bootstrap = function(cb) {
  sails.services.passport.loadStrategies();

  var soap = require('soap-server');

  function MyTestService(){
  }
  MyTestService.prototype.test1 = function(id, name){
    Project.update({id:id},{name:name})
      .exec(function (err, project) {
        return project;
      });
  };

  var soapServer = new soap.SoapServer();
  var soapService = soapServer.addService('testService', new MyTestService());

  soapServer.listen(1338, '127.0.0.1');

  // It's very important to trigger this callback method when you are finished
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
  cb();
};
