# pxapp

Pionix test app

1. Clone the repository
2. cd pionixtest/
3. `npm install`
4. `mv 'config/env/development.js.sample' 'config/env/development.js'`
5. Edit development.js
6. run with command `npm start app.js` or `sails lift`

Redis config is in 'config/session.js'
SOAP server will be available on localhost on port 1338
