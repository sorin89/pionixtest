/**
 * ProjectController
 *
 * @description :: Server-side logic for managing projects
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	get: function (req, res, next) {
		var id = req.param('id');
		var moment = require('moment');
		Project.findOne({id:id})
			.exec(function(err, project) {
				if(err) return next(err);
				res.view({project:project, moment:moment});
			});
	},
	delete: function (req, res, next) {
		var id = req.param('id');
		Project.destroy({id:id})
			.exec(function(err, project) {
				if(err) return next(err);
				res.json({ok:'1'});
			});
	},
	create: function (req, res, next) {
		var name = req.param('name');
		Project.create({name:name, user:req.user.id})
			.exec(function(err, project) {
				if(err) return next(err);
				res.json(project);
			});
	},
	update: function (req, res, next) {
		var id = req.param('id'),
		    name = req.param('name');
		var easysoap = require('easysoap');
    var params = {
  		host   : 'http://127.0.0.1:1338',
  		path   : '/testService',
      wsdl   : '/testService?wsdl',
    };
    var soapClient = easysoap.createClient(params);
    //soapClient.getAllFunctions()
    //	.then((functionArray) => { console.log(functionArray); })
		//	.catch((err) => { throw new Error(err); });
    soapClient.call({
      method    : 'test1',
      params : {id: id, name: name}
    })
    .then((callResponse) => {
			//console.log(callResponse.data);	// response data as json
        })
		.catch((err) => { throw new Error(err); });
	},
};
