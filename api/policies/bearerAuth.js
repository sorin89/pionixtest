/**
 * bearerAuth Policy
 *
 * Policy for authorizing API requests. The request is authenticated if the
 * it contains the accessToken in header, body or as a query param.
 */

module.exports = function (req, res, next) {

  return passport.authenticate('bearer', { session: false })(req, res, next);

};
