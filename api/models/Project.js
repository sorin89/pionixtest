/**
 * Project.js
 *
 * Project model
 */

module.exports = {

  attributes: {
    name : { type: 'string', required: true },
    user : {model: 'user', required: true }
  }
};
